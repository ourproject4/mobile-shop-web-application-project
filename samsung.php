<?php
    include("managecart.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="mycart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="managecart.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="download (3).jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Galaxy A03s(4GB RAM)</h6>
						    <p class="card-text">Price: Rs.16000</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Galaxy A03s(4GB RAM)">
						    <input type="hidden" name="Price" value="16000">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managecart.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="download .jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Galaxy M12</h6>
						    <p class="card-text">Price: Rs.16300</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Galaxy M12">
						    <input type="hidden" name="Price" value="16300">
						    
						    </div>
						</div>
					</form>
				</div>

                <div class="col-lg-3">
					<form action="managecart.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="download (1).jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Galaxy M01s</h6>
						    <p class="card-text">Price: Rs.9999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Galaxy M01s">
						    <input type="hidden" name="Price" value="9999">
						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managecart.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="download.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Galaxy S21</h6>
						    <p class="card-text">Price: Rs.50999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Galaxy S21">
						    <input type="hidden" name="Price" value="50999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managecart.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="images (1).jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Galaxy S21 5G 256GB Grey</h6>
						    <p class="card-text">Price: Rs.50999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Galaxy S21 5G 256GB Grey">
						    <input type="hidden" name="Price" value="50999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managecart.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="galaxyf23.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Galaxy F23 6GB 128GB</h6>
						    <p class="card-text">Price: Rs.45999</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Galaxy F23 6GB 128GB">
						    <input type="hidden" name="Price" value="45999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managecart.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="samsungs20fe.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">GALAXY S20FE </h6>
						    <p class="card-text">Price: Rs.39999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="GALAXY S20FE">
						    <input type="hidden" name="Price" value="39999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managecart.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="galaxy F22.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Galaxy F22</h6>
						    <p class="card-text">Price: Rs.14999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Galaxy F22">
						    <input type="hidden" name="Price" value="14999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managecart.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="galaxyS22.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Galaxy S22</h6>
						    <p class="card-text">Price: Rs.118999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Galaxy S22">
						    <input type="hidden" name="Price" value="118999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managecart.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="S20 fe 5G.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">S20 Fe 5G</h6>
						    <p class="card-text">Price: Rs.30499</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="S20 Fe 5G">
						    <input type="hidden" name="Price" value="30499">
						    </div>
						</div>
					</form>
				</div>


</body>
</html>