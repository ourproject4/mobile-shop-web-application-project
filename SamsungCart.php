

<!DOCTYPE html>
<html>
	<head>
		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

		<title>Login</title>
	</head>

	<body>

		<!-- #Negavation_Bar -->
           <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="logout.php">Logout</a>
          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
              </div>
            </nav>
            <body class="container">
                <h3 class="text-center text-danger mb-5" style="font-family: 'Abril fatface', cursive;"> SAMSUNG MOBILE </h3>

                <div class="row">
                <?php
                    $conn = mysqli_connect('localhost','root');
                    mysqli_select_db($conn,'shoppingcart');

                    if($conn){
                        echo "connection successfull";

                    }else{
                        echo "no connection ";
                    }
                    $query = "SELECT `name`, `image`, `price`, `discount` FROM `shoppingcart` order by id ASC ";
                    
                    $queryfire = mysqli_query($conn, $query);

                    $num = mysqli_num_rows($query);

                    if($num > 0){
                        while($product = mysqli_fetch_array($queryfire)){
                            ?>
                              
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <form>
                                    <div class="card">
                                        <h6 class="card-title bg-info text-while p-2 text-uppercase"> <?php echo 
                                        $product['name'];   ?> </h6>

                                    <div class="card-body">  
                                        <img src="<?php echo
                                        $product['image']; ?>" alt="phone" class="img-fluid mb-2">

                                        <h6> &#8377; <?php echo $product['price']; ?><span> 
                                        (<?php echo $product['discount']; ?>% off) </span></h6>

                                        <h6 class="badge badge-success"> 4.4 <i class="fa fa-star"></i></h6> 

                                        <input type="text" name="" class="form-control" placeholder="Quantity">
                                    </div>
                                    <div class="btn-group d-flex">
                                        <button class="btn btn-success flex-fill"> Add to Cart</button><button class="btn btn-warning flex-fill text-white"> Buy Now</button>
                                    </div>
                                </form>                                
                            </div>
                          <?php
                        }
                    }


                ?>



            </body>
            </html>