<?php
   
    include("boatbudsmanage.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="boatbudscart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="boatbudsmanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="boatbud1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt Airdopes 131 RTL In-Ear Truly Wireless Earbuds</h6>
						    <p class="card-text">Price: Rs.999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Airdopes 131 RTL In-Ear Truly Wireless Earbuds">
						    <input type="hidden" name="Price" value="999">						    
						    </div>
						</div>
					</form>
				</div>

				
               

				<div class="col-lg-3">
					<form action="boatbudsmanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="boatbud2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt Airdopes 381 RTL In-Ear Truly Wireless Earbud</h6>
						    <p class="card-text">Price: Rs.1599</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Airdopes 381 RTL In-Ear Truly Wireless Earbud">
						    <input type="hidden" name="Price" value="1599">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="boatbudsmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="boatbud3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">BOAT AIRDOPES 141 - BEST WIRELESS EARBUDS ONLINE CYAN CIDER </h6>
						    <p class="card-text">Price: Rs.1999</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="BOAT AIRDOPES 141 - BEST WIRELESS EARBUDS ONLINE CYAN CIDER">
						    <input type="hidden" name="Price" value="1999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="boatbudsmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="boatbud4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt Airdopes 111</h6> 
						    <p class="card-text">Price: Rs.1359</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Airdopes 111">
						    <input type="hidden" name="Price" value="1359">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="boatbudsmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="boatbud5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">BOAT Airdopes 121 v2</h6>
						    <p class="card-text">Price: Rs.1299</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="BOAT Airdopes 121 v2">
						    <input type="hidden" name="Price" value="1299">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="boatbudsmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="boatbud6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt Airdopes 141</h6>
						    <p class="card-text">Price: Rs.1499</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Airdopes 141">
						    <input type="hidden" name="Price" value="1499">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="boatbudsmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="boatbud7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">BOAT AIRDOPES 101 M TRUE WIRELESS EARBUDS ACTIVE BLACK (ONESIZE)</h6>
						    <p class="card-text">Price: Rs.1000</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="BOAT AIRDOPES 101 M TRUE WIRELESS EARBUDS ACTIVE BLACK (ONESIZE)">
						    <input type="hidden" name="Price" value="1000">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="boatbudsmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="boatbud8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt Airdopes 131 Bluetooth Headset (Active Black, True Wireless)</h6>
						    <p class="card-text">Price: Rs.699</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Airdopes 131 Bluetooth Headset (Active Black, True Wireless)">
						    <input type="hidden" name="Price" value="699">
						    </div>
						</div>
					</form>
				</div>
                
                <div class="col-lg-3">
					<form action="boatbudsmanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="boatbud9.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt Airdopes 121v2 True Wireless Earbuds </h6>
						    <p class="card-text">Price: Rs.1599</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Airdopes 121v2 True Wireless Earbuds ">
						    <input type="hidden" name="Price" value="1599">
						    
						    </div>
						</div>
					</form>
				</div>

</body>
</html>