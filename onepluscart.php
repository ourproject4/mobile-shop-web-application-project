<?php 
   include("manageoneplus.php");
   include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<title>cart</title>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
	.btn btn-outline danger
	{
		padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    letter-spacing: 2px;
	    height: 32px;
	}
	.btn-outline-danger
	{
		padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    color: #FFFFFF;
	    border: solid;
	    text-transform: uppercase;
	    font-weight: 500;
	    position: absolute;
	    top: 10px;
	    right: 120px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="oneplus.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                  </li>
                </ul>
                <div>
                	<a href="userorders.php" class="btn-sm btn-outline-danger">Orders</a>
                </div>

                <div>
                	<a href="onepluscart.php" class="btn btn-outline-success">My Cart</a>
                </div>
              </div>
            </nav>
    <form action="onepluscart.php" method="post">    
	<div class="container">
	<div class="row">
		<div class="col-lg-12 text-center border rounded bg-light my-5">
			<h3>MY CART</h3>

		</div>
		<div class="col-lg-8">
			<table class="table">
			  <thead class="text-center">
			    <tr>
			      <th scope="col">Serial No.</th>
			      <th scope="col">Item Name</th>
			      <th scope="col">Item Price</th>
			      <th scope="col">Quantity</th>
			    </tr>
			  </thead>
			  <tbody class="text-center">
			  	<?php
			  	$total=0;
			  	if(isset($_SESSION['cart']))
			  	{
                    foreach($_SESSION['cart'] as $key => $value)
                    {
                    	$sr=$key+1;
                    	$total=$total+$value['Price'];
                    	echo "
                    	<tr>
                    	 <td>$sr</td>
                    	 <td>$value[Item_Name]</td>
                    	 <td>$value[Price]<input type='hidden' class='iprice' value='$value[Price]'></td>
                    	 <td>
                         <form action='manageoneplus.php' method='POST'>
                    	 <input class='text-center iquantity' onchange='subTotal()' type='number' value='$value[Quantity]' min='1' max='10'>
                    	 <input type='hidden' name='Item_Name' value='$value[Item_Name]'>
                         </form>
                    	 </td>
                    	 <td class='itotal'></td>
                    	 <td>
                    	 <form action='manageoneplus.php' method='POST'>
                    	 <button name='Remove_Item' class='btn btn-sm btn-primary btn-block'>REMOVE</button>
                    	 <input type='hidden' name='Item_Name' value='$value[Item_Name]'>
                    	 </form>
                    	 </td>
                    	 </tr>
                    	 ";
                    }
                }
			  	?>		    
			  </tbody>
			</table>
		</div>
	</form>
         
        <div class="col-lg-4">
        	<div class="border bg-light rounded p-4">
        <h4>Grand Total:</h4>
        <h5 class="text-right" id="gtotal"><?php echo $total ?></h5>
        <br>
        <?php
              if(isset($_SESSION['cart'])  && count($_SESSION['cart'])>0)
              { 
        ?>
        <form action="onepluscart.php" method="POST">
        	<div class="form-group">
			    <label>Full Name</label>
			    <input type="text" name="Full_Name" class="form-control">
			</div>
			<div class="form-group">
			    <label>Phone Number</label>
			    <input type="number" name="Phone_No" class="form-control">
			</div>
			<div class="form-group">
			    <label>Address</label>
			    <input type="text" name="Address" class="form-control">
			</div>

			<div class="form-check">
			  <input class="form-check-input" type="radio" name="Pay_Mode" value="COD" id="flexRadioDefault2" checked>
			  <label class="form-check-label" for="flexRadioDefault2">
			    Cash on delivery
			  </label>
			</div>
			<br>
        	<button class="btn btn-primary btn-block" name="purchase">Make Purchase</button>
        </form>
        <?php 
             }
        ?>
            </div>
        </div>
	</div>   
</div>
<script>
	var gt=0;
	var iprice=document.getElementByClassName('iprice');
	var iquantity=document.getElementByClassName('iquantity');
	var itotal=document.getElementByClassName('itotal');
	var gtotal=document.getElementById('gtotal');
</script>
</body>
</html>