<?php
   
    include("wireoppomanage.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="wireoppocart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="wireoppomanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppo1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Wired Rich Bass 3.5mm Headset/Earphone</h6>
						    <p class="card-text">Price: Rs.264</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Wired Rich Bass 3.5mm Headset/Earphone">
						    <input type="hidden" name="Price" value="264">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wireoppomanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppo2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">ORIGINAL OPPO EARPHONES</h6>
						    <p class="card-text">Price: Rs.99</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="ORIGINAL OPPO EARPHONES">
						    <input type="hidden" name="Price" value="99">
						    
						    </div>
						</div>
					</form>
				</div>

                <div class="col-lg-3">
					<form action="wireoppomanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oppo3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title"> Oppo Earphone for Oppo Mobiles</h6>
						    <p class="card-text">Price: Rs.407</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo Earphone for Oppo Mobiles">
						    <input type="hidden" name="Price" value="407">
						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wireoppomanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oppo4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OPPO MH319 Deep Bass Wired Earphone with Mic</h6>
						    <p class="card-text">Price: Rs.899</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OPPO MH319 Deep Bass Wired Earphone with Mic">
						    <input type="hidden" name="Price" value="899">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wireoppomanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppo5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo Buds Classic Wired Earphones with HD Microphone Wired</h6>
						    <p class="card-text">Price: Rs.122</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo Buds Classic Wired Earphones with HD Microphone Wired">
						    <input type="hidden" name="Price" value="122">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wireoppomanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="oppo6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">oppo 0.35mm Jack White Earphone</h6>
						    <p class="card-text">Price: Rs.99</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="oppo 0.35mm Jack White Earphone">
						    <input type="hidden" name="Price" value="99">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wireoppomanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oppo7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo super ultra wired op_po earphone </h6>
						    <p class="card-text">Price: Rs.69</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo super ultra wired op_po earphone ">
						    <input type="hidden" name="Price" value="69">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wireoppomanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oppo8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">oppo earphone 3.5 mmOppo</h6>
						    <p class="card-text">Price: Rs.99</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="oppo earphone 3.5 mmOppo">
						    <input type="hidden" name="Price" value="99">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wireoppomanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oppo9.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">	oppo original quality earphones with boxBSC</h6>
						    <p class="card-text">Price: Rs.199</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="oppo original quality earphones with boxBSC">
						    <input type="hidden" name="Price" value="199">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wireoppomanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppo10.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Rv Praman Earphone 3.5mm for OpPo</h6>
						    <p class="card-text">Price: Rs.299</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Rv Praman Earphone 3.5mm for OpPo">
						    <input type="hidden" name="Price" value="299">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wireoppomanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppo11.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OPPO ORIGINAL EARPHONES 3.5 MMBT-BOAT</h6>
						    <p class="card-text">Price: Rs.99</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OPPO ORIGINAL EARPHONES 3.5 MMBT-BOAT">
						    <input type="hidden" name="Price" value="99">
						    </div>
						</div>
					</form>
				</div>


</body>
</html>