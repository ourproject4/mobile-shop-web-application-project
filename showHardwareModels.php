<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
	  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
	  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
	  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
	<title>Show Hardware Models</title>
</head>
<body>
	<div class="container"> <br>
		<h1 class="text-center text-white bg-dark"> Show Hardwares Models</h1> <br>
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover text-center">
				<thead>
					<th> Model Id </th>
					<th> Hardware Id </th>
					<th> Rack </th>
					<th> Price </th>
					<th> Status </th>
					<th> Image </th>

					<tbody>
						<?php
						// include 'hardware.php';
							$server = "localhost";
						    $username = "root";
						    $password = "";

						    // Create a database connection
						    $con = mysqli_connect($server, $username, $password);

						    $displayquery = "select * from `role`.`hardwaremodels`";
						    $querydisplay = mysqli_query($con, $displayquery);

						    // $row = mysqli_num_rows($querydisplay);

						    while ($result = mysqli_fetch_array($querydisplay)) {

						    	?>

						    	<tr>
						    		<td> <?php echo $result['ModelId']; ?> </td>
						    		<td> <?php echo $result['HardwareId']; ?> </td>
						    		<td> <?php echo $result['rack']; ?> </td>
						    		<td> <?php echo $result['price']; ?> </td>
						    		<td> <?php echo $result['status']; ?> </td>
						    		<td> <img src="<?php echo $result['image']; ?> " height="100px" width="100px"> </td>
						    	</tr>

						    <?php
						    }
						?>
					</tbody>
				</thead>
			</table>
		</div>
	</div>
</body>
</html>