<?php
   
    include("managesambuds.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="sambudscart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="managesambuds.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="sambuds1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Galaxy Buds Pro SM-R190NZKAINU In-Ear</h6>
						    <p class="card-text">Price: Rs.8499</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Galaxy Buds Pro SM-R190NZKAINU In-Ear">
						    <input type="hidden" name="Price" value="8499">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesambuds.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="sambuds2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Spigen Metal Sticker Shine Shield For Samsung Galaxy</h6>
						    <p class="card-text">Price: Rs.690</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Spigen Metal Sticker Shine Shield For Samsung Galaxy">
						    <input type="hidden" name="Price" value="690">
						    
						    </div>
						</div>
					</form>
				</div>

               

				<div class="col-lg-3">
					<form action="managesambuds.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="sambuds3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Galaxy Buds Live</h6>
						    <p class="card-text">Price: Rs.6999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Galaxy Buds Live">
						    <input type="hidden" name="Price" value="6999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesambuds.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="sambuds4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Bluetooth In-Ear Earbuds With Mic</h6>
						    <p class="card-text">Price: Rs.1799</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Bluetooth In-Ear Earbuds With Mic">
						    <input type="hidden" name="Price" value="1799">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesambuds.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="sambuds5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Galaxy Buds2(Graphite)</h6> 
						    <p class="card-text">Price: Rs.11999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Galaxy Buds2(Graphite)">
						    <input type="hidden" name="Price" value="11999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesambuds.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="sambuds6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Galaxy Buds-Live Noise-Canceling True Wireless Earbud Headphones</h6>
						    <p class="card-text">Price: Rs.4990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Galaxy Buds-Live Noise-Canceling True Wireless Earbud Headphones">
						    <input type="hidden" name="Price" value="4990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesambuds.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="sambuds7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Buds Pro Og Quality</h6>
						    <p class="card-text">Price: Rs.1090</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Buds Pro Og Quality">
						    <input type="hidden" name="Price" value="1090">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesambuds.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="sambuds8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Galaxy Buds Pro SM-R190NZSAINU In-Ear Truly Wireless Earbuds with Mic</h6>
						    <p class="card-text">Price: Rs.8999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Galaxy Buds Pro SM-R190NZSAINU In-Ear Truly Wireless Earbuds with Mic">
						    <input type="hidden" name="Price" value="8999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesambuds.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="sambuds9.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Galaxy Ear Buds Pro (Black) | infiswap</h6>
						    <p class="card-text">Price: Rs.8909</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Galaxy Ear Buds Pro (Black) | infiswap">
						    <input type="hidden" name="Price" value="8909">
						    </div>
						</div>
					</form>
				</div>
                
                
</body>
</html>