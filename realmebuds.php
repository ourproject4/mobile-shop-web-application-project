<?php
   
    include("budsrealmemanage.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="realmecartbuds.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="budsrealmemanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realmebuds1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme Buds Air 2 RMA2003</h6>
						    <p class="card-text">Price: Rs.3299</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme Buds Air 2 RMA2003">
						    <input type="hidden" name="Price" value="3299">						    
						    </div>
						</div>
					</form>
				</div>

				
               

				<div class="col-lg-3">
					<form action="budsrealmemanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realmebuds3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme Buds Air Pro RMA210</h6>
						    <p class="card-text">Price: Rs.4999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme Buds Air Pro RMA210">
						    <input type="hidden" name="Price" value="4999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="budsrealmemanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="realmebuds4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">realme Buds Air 2 Truly Wireless Earbuds </h6>
						    <p class="card-text">Price: Rs.3299</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="realme Buds Air 2 Truly Wireless Earbuds ">
						    <input type="hidden" name="Price" value="3299">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="budsrealmemanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="realmebuds5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">realme buds Q Neo True Wireless </h6> 
						    <p class="card-text">Price: Rs.1399</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="realme buds Q Neo True Wireless ">
						    <input type="hidden" name="Price" value="1399">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="budsrealmemanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="realmebuds6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">realme Buds Air 2 with Active Noise Cancellation (ANC) Bluetooth Headset</h6>
						    <p class="card-text">Price: Rs.2990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="realme Buds Air 2 with Active Noise Cancellation (ANC) Bluetooth Headset">
						    <input type="hidden" name="Price" value="2990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="budsrealmemanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="realmebuds7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">realme Buds Air Neo Bluetooth Headset </h6>
						    <p class="card-text">Price: Rs.1290</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="realme Buds Air Neo Bluetooth Headset">
						    <input type="hidden" name="Price" value="1290">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="budsrealmemanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="realmebuds8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme Neo true wireless earbuds</h6>
						    <p class="card-text">Price: Rs.2299</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme Neo true wireless earbuds">
						    <input type="hidden" name="Price" value="2299">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="budsrealmemanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="realmebuds9.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme Buds Air Pro</h6>
						    <p class="card-text">Price: Rs.899</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme Buds Air Pro">
						    <input type="hidden" name="Price" value="899">
						    </div>
						</div>
					</form>
				</div>
                
                <div class="col-lg-3">
					<form action="budsrealmemanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realmebuds2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme Buds Q2 Neo Earbuds with Instant Connection, IPX4 Sweat and Water Resistant, Black</h6>
						    <p class="card-text">Price: Rs.1599</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme Buds Q2 Neo Earbuds with Instant Connection, IPX4 Sweat and Water Resistant, Black">
						    <input type="hidden" name="Price" value="1599">
						    
						    </div>
						</div>
					</form>
				</div>

</body>
</html>