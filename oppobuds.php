<?php
   
    include("oppobudsmanage.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="oppobudscart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="oppobudsmanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppobuds1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo Enco Buds True Wireless Earbuds</h6>
						    <p class="card-text">Price: Rs.1299</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo Enco Buds True Wireless Earbuds">
						    <input type="hidden" name="Price" value="1299">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="oppobudsmanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppobuds2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo Enco buds</h6>
						    <p class="card-text">Price: Rs.1799</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo Enco buds">
						    <input type="hidden" name="Price" value="1799">
						    
						    </div>
						</div>
					</form>
				</div>

               

				<div class="col-lg-3">
					<form action="oppobudsmanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppobuds3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo Enco Buds TWS</h6>
						    <p class="card-text">Price: Rs.1799</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo Enco Buds TWS">
						    <input type="hidden" name="Price" value="1799">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="oppobudsmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="oppobuds4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo Enco free</h6>
						    <p class="card-text">Price: Rs.2499</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo Enco free">
						    <input type="hidden" name="Price" value="2499">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="oppobudsmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oppobuds5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo Enco free</h6> 
						    <p class="card-text">Price: Rs.2399</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo Enco free">
						    <input type="hidden" name="Price" value="2399">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="oppobudsmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oppobuds6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OPPO ENCO W31 WIRELESS EARBUD, BLACK</h6>
						    <p class="card-text">Price: Rs.1990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OPPO ENCO W31 WIRELESS EARBUD, BLACK">
						    <input type="hidden" name="Price" value="1990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="oppobudsmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oppobuds8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo Enco Air W32 True Wireless Headphones </h6>
						    <p class="card-text">Price: Rs.4940</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo Enco Air W32 True Wireless Headphones ">
						    <input type="hidden" name="Price" value="4940">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="oppobudsmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oppobuds7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo Enco with Active voice</h6>
						    <p class="card-text">Price: Rs.9990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo Enco with Active voice">
						    <input type="hidden" name="Price" value="9990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="oppobudsmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oppobuds9.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo Enco W51 True Wireless Bluetooth Headphones in-Ear</h6>
						    <p class="card-text">Price: Rs.10708</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo Enco W51 True Wireless Bluetooth Headphones in-Ear">
						    <input type="hidden" name="Price" value="10708">
						    </div>
						</div>
					</form>
				</div>
                
                
</body>
</html>