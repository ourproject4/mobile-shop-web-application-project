<?php 
session_start();

if (isset($_SESSION['id']) && isset($_SESSION['username'])) {
   include "config1.php";
   //include "changepass_wd.php";
}
 ?>
<!DOCTYPE html>
<html>
<head>
   <title>Change Password</title>
   <link rel="stylesheet" type="text/css" href="style.css">
   <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
   
    <form action="changepass_wd.php" method="post">
      <?php if (isset($_GET['error'])) { ?>
         <p class="error"><?php echo $_GET['error']; ?></p>
      <?php } ?>

      <?php if (isset($_GET['success'])) { ?>
            <p class="success"><?php echo $_GET['success']; ?></p>
        <?php } ?>
      <center>
        <div class="popup-container mt-4">
            <div class="popup">
    <div class="col-sm-4">
        <div class="card">
      <h4>Change Password</h4>
      
      <div class="card-body">
      <hr class="mb-1">
      
      <input type="password" 
             class="form-control"
             name="op" 
             placeholder="Old Password" required>
             <br>

   
      <input type="password" 
             class="form-control"
             name="np" 
             placeholder="New Password" required>
             <br>

     
      <input type="password" 
             class="form-control"
              name="c_np" 
             placeholder="Confirm New Password" required>
             <br>

      <button type="submit" class="btn btn-primary">CHANGE PASSWORD</button>
          <a href="home.php" class="ca">HOME</a>
       </div>
    </div>
 </div>
     </form>
</body>
</html>

