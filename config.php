<?php
    /*
    #This_file_contains_database_configuration_like_#servaer_name_#username_#password_#DB_name
    */

    define('DB_SERVER', 'localhost');
    define('DB_USERNAME', 'root');
    define('DB_PASSWORD', '');
    define('DB_NAME', 'role');

    // #Try_connecting_to_the_Database
    $conn = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

    // #Check_the_connection
    if($conn == false){
        dir('Error: Cannot connect');
    }
?>