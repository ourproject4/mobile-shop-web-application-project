<?php 
  
   include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<title>cart</title>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
	.btn-sm btn-outline-danger
	{
		padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                  </li>
                </ul>
            </ul>
                <div>
                	<a href="mycart.php" class="btn btn-outline-success">My Cart</a>
                </div>
              </div>
            </nav>
        
        <div class="container mt-5">
        	<div class="row">
            <div class="col-lg-12">
            <table class="table text-center table-dark table-striped">
			  <thead>
			    <tr>
			      <th scope="col">Order ID</th>
			      <th scope="col">Customer Name</th>
			      <th scope="col">Phone No.</th>
			      <th scope="col">Address</th>
			      <th scope="col">Pay_Mode</th>
			      <th scope="col">Orders</th>
			    </tr>
			  </thead>
			  

			  	<?php 
                     $query="SELECT * FROM `orderplace`";
                     $user_result=mysqli_query($con,$query);
                     while($user_fetch=mysqli_fetch_assoc($user_result))
                     {
                     	echo"
                            <tr>
						      <td>$user_fetch[Order_id]</td>
						      <td>$user_fetch[Full_Name]</td>
						      <td>$user_fetch[Phone_No]</td>
						      <td>$user_fetch[Address]</td>
						      <td>$user_fetch[Pay_Mode]</td>
                              <td>
                                  <table class='table text-center table-dark table-striped'>
									  <thead>
									    <tr>
									      <th scope='col'>Item Name</th>
									      <th scope='col'>Price</th>
									      <th scope='col'>Quantity</th>
									  
									    </tr>
									  </thead>
									  <tbody>
									  ";
						$order_query="SELECT * FROM `user_orders` WHERE 'Order_id'='$user_fetch[Order_id]'";
						$order_result=mysqli_query($con,$order_query);
						while($order_fetch=mysqli_fetch_assoc($order_result))
						{
							echo"
                                <tr>
                                  <td>$order_fetch[Item_Name]</td>
                                  <td>$order_fetch[Price]</td>
                                  <td>$order_fetch[Quantity]</td>
                                </tr>
							";
						}

						echo"
                                           </tbody>
                                           </table>
                                           </td>
                                           </tr>
									  ";                             					    
		                     }
					  	?>		    
					   </tbody>
					  </table>
					 </div>
					</div>
				</div>


</body>
</html>            