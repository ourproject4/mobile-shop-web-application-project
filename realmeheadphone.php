<?php
   
    include("managerealmehead.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="realmeheadcart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="managerealmehead.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realme1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme Headphones With Extra Bass</h6>
						    <p class="card-text">Price: Rs.1000</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme Headphones With Extra Bass">
						    <input type="hidden" name="Price" value="1000">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managerealmehead.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realme2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme Wirless Headphones With Mic</h6>
						    <p class="card-text">Price: Rs.1050</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme Wirless Headphones With Mic">
						    <input type="hidden" name="Price" value="1050">
						    
						    </div>
						</div>
					</form>
				</div>

               

				<div class="col-lg-3">
					<form action="managerealmehead.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realme3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Razer Kraken Gaming Headset</h6>
						    <p class="card-text">Price: Rs.9001</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Razer Kraken Gaming Headset">
						    <input type="hidden" name="Price" value="9001">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managerealmehead.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="realme4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">SteelSeries Arctis Pro Wireless - Gaming Headset</h6>
						    <p class="card-text">Price: Rs.41344</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="SteelSeries Arctis Pro Wireless - Gaming Headset">
						    <input type="hidden" name="Price" value="41344">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managerealmehead.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="realme5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Bose 700 Bluetooth Wireless On-Ear Headphones with Mic - Noise-Canceling - Luxe Silver</h6> <p class="card-text">Price: Rs.47605</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Bose 700 Bluetooth Wireless On-Ear Headphones with Mic - Noise-Canceling - Luxe Silver">
						    <input type="hidden" name="Price" value="47605">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managerealmehead.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="realme6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Corsair Virtuoso RGB 7.1 Wireless Headphones</h6>
						    <p class="card-text">Price: Rs.40105</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Corsair Virtuoso RGB 7.1 Wireless Headphones">
						    <input type="hidden" name="Price" value="40105">
						    </div>
						</div>
					</form>
				</div>
                
                
</body>
</html>