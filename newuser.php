<?php
if (isset($_POST['submit'])){
    // Set connection variables
    $server = "localhost";
    $username = "root";
    $password = "";

    // Create a database connection
    $con = mysqli_connect($server, $username, $password);

    // Check for connection success
    if(!$con){
        die("connection to this database failed due to" . mysqli_connect_error());
    }
    // echo "Success connecting to the db";

    // Collect post variables
    $username = $_POST['username'];
    $password = $_POST['password'];
    
    $sql = "INSERT INTO `role`.`login` (`username`, `password`) VALUES ('$username', '$password');";
    // echo $sql;

    // Execute the query
    if($con->query($sql) == true){
        echo "Successfully inserted";

        // Flag for successful insertion
        $insert = true;
    }
    else{
        echo "ERROR: $sql <br> $con->error";
    }

    // Close the database connection
    $con->close();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="style.css">
    <title>New User</title>
</head>
<body>
      <!-- #Negavation_Bar -->
           <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="logout.php">Logout</a>
          </li>
                    
                  </li>
                </ul>
              </div>
            </nav>
<center>
            <div class="container">
            <form accept="newuser.php" method="post">
                <div class="popup-container mt-4">
                    <div class="popup">
                        <div class="col-sm-4">
                            <div class="card">
                                <h5>User SignUp </h5>
                                Fill up the form with correct values                               
                                <div class="card-body">                        
                                    <hr class="mb-1"><br>
        
                <input type="text" class="form-control" name="username" id="username" placeholder="Enter username" required> <br> 
               <div class="form-group"> 
            <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" required> 
            </div> 
            <input type="submit" class="btn btn-primary" name="SignUp" value="SignUp">
            <br> <br>
            <p class="login-register-text">Login your account <b><a href="login.php">Login here</a>.</b></p>
        </form>
    </div>
</center>
</body>
</html>

