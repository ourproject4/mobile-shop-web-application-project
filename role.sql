-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2022 at 12:54 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `role`
--

-- --------------------------------------------------------

--
-- Table structure for table `hardware`
--

CREATE TABLE `hardware` (
  `HardwareId` int(100) NOT NULL,
  `Hardwarename` varchar(50) NOT NULL,
  `Companyname` varchar(50) NOT NULL,
  `Hardwaretype` varchar(50) NOT NULL,
  `image` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hardware`
--

INSERT INTO `hardware` (`HardwareId`, `Hardwarename`, `Companyname`, `Hardwaretype`, `image`) VALUES
(1, '', 'zara', 'earphone', ''),
(2, '', 'zara', 'earphone', ''),
(3, '', 'boat', 'charger', ''),
(4, 'headphone', 'boat', 'headphone', ''),
(5, '', '', '', ''),
(6, 'charger', 'lkjhgf', 'poiuyt', ''),
(7, '', '', '', 0x4861726477617265506963732f776972656465617270686f6e652e6a7067),
(8, '', '', '', 0x4861726477617265506963732f426c7565746f6f74682e706e67),
(9, '', '', '', 0x4861726477617265506963732f426c7565746f6f74682e706e67),
(10, '', '', '', 0x4861726477617265506963732f426c7565746f6f74682e706e67),
(11, '', '', '', 0x4861726477617265506963732f776972656465617270686f6e652e6a7067),
(12, '', '', '', 0x4861726477617265506963732f636861726765722e6a7067);

-- --------------------------------------------------------

--
-- Table structure for table `hardwaremodels`
--

CREATE TABLE `hardwaremodels` (
  `ModelId` int(11) NOT NULL,
  `HardwareId` int(11) NOT NULL,
  `rack` varchar(50) NOT NULL,
  `price` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Available',
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hardwaremodels`
--

INSERT INTO `hardwaremodels` (`ModelId`, `HardwareId`, `rack`, `price`, `status`, `image`) VALUES
(0, 1, '2', '2000', '', ''),
(0, 4, '3', '2000', 'Available', ''),
(0, 5, '4', '300', 'Available', '');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(10) NOT NULL DEFAULT 'owner'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `role`) VALUES
(1, 'owner', 'owner', 'owner'),
(2, 'manager', 'manager', 'manager'),
(3, 'user', 'user', 'user'),
(16, 'rohini', 'rohi', 'owner'),
(17, 'vishal', 'vishal', 'owner'),
(18, 'vishal', 'vishal', 'owner'),
(19, 'vishal', 'vishal', 'owner'),
(20, 'user1', 'user1', 'owner'),
(21, '', '', 'owner');

-- --------------------------------------------------------

--
-- Table structure for table `orderplace`
--

CREATE TABLE `orderplace` (
  `Order_id` int(100) NOT NULL,
  `Full_Name` text NOT NULL,
  `Phone_No` bigint(100) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `Pay_Mode` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderplace`
--

INSERT INTO `orderplace` (`Order_id`, `Full_Name`, `Phone_No`, `Address`, `Pay_Mode`) VALUES
(1, 'vaishu', 654123987, 'abc', 'COD'),
(2, '', 0, '', ''),
(3, 'devyani', 321456987, 'klm', 'COD'),
(4, 'aditya', 321456987, 'abc', 'COD'),
(5, 'shree', 654789321, 'lmkjh', 'COD'),
(6, 'snehal', 654123987, 'lkj', 'COD'),
(7, '', 0, '', 'COD'),
(8, 'sejal', 632145987, 'lkj', 'COD'),
(9, 'raghav', 90876543, 'bnm', 'COD'),
(10, 'soham', 321456987, 'asd', 'COD'),
(11, 'sahil', 32146987, 'mno', 'COD'),
(12, 'prasad', 321469875, 'dsa', 'COD');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `image` longblob NOT NULL,
  `datetime` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `discount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shoppingcart`
--

INSERT INTO `shoppingcart` (`id`, `name`, `image`, `price`, `discount`) VALUES
(1, 'Samsung', 'download (1).jpg', 18000, 90),
(2, 'samsung', 'download (3).jpg', 15000, 50);

-- --------------------------------------------------------

--
-- Table structure for table `user_orders`
--

CREATE TABLE `user_orders` (
  `Order_id` int(100) NOT NULL,
  `Item_Name` varchar(100) NOT NULL,
  `Price` int(100) NOT NULL,
  `Quantity` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hardware`
--
ALTER TABLE `hardware`
  ADD PRIMARY KEY (`HardwareId`);

--
-- Indexes for table `hardwaremodels`
--
ALTER TABLE `hardwaremodels`
  ADD PRIMARY KEY (`HardwareId`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderplace`
--
ALTER TABLE `orderplace`
  ADD PRIMARY KEY (`Order_id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hardware`
--
ALTER TABLE `hardware`
  MODIFY `HardwareId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `orderplace`
--
ALTER TABLE `orderplace`
  MODIFY `Order_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
