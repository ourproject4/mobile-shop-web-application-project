<?php
   
    include("manageoneplus.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="onepluscart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="manageoneplus.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oneplus 7pro.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus 7 pro</h6>
						    <p class="card-text">Price: Rs.57999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus 7 pro">
						    <input type="hidden" name="Price" value="57999">						    
						    </div>
						</div>
					</form>
				</div>


				<div class="col-lg-3">
					<form action="manageoneplus.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="oneplus Nord CE.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus Nord CE</h6>
						    <p class="card-text">Price: Rs.23999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus Nord CE">
						    <input type="hidden" name="Price" value="23999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoneplus.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oneplus Nord.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus Nord</h6>
						    <p class="card-text">Price: Rs.23990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus Nord">
						    <input type="hidden" name="Price" value="23990">
						    </div>
						</div>
					</form>
				</div>


				<div class="col-lg-3">
					<form action="manageoneplus.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="oneplus7T.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus 7T</h6>
						    <p class="card-text">Price: Rs.37990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus 7T">
						    <input type="hidden" name="Price" value="37990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoneplus.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="oneplus8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus 8</h6>
						    <p class="card-text">Price: Rs.16990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus 8">
						    <input type="hidden" name="Price" value="16990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoneplus.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oneplus8pro.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus 8 pro</h6>
						    <p class="card-text">Price: Rs.52999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus 8 pro">
						    <input type="hidden" name="Price" value="52999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoneplus.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oneplus9.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus 9</h6>
						    <p class="card-text">Price: Rs.49990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus 9">
						    <input type="hidden" name="Price" value="49990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoneplus.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oneplus9R.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus 9R</h6>
						    <p class="card-text">Price: Rs.37990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus 9R">
						    <input type="hidden" name="Price" value="37990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoneplus.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="oneplus9RT.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus 9RT</h6>
						    <p class="card-text">Price: Rs.42990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus 9RT">
						    <input type="hidden" name="Price" value="42990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoneplus.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="oneplus10pro.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus 10 pro</h6>
						    <p class="card-text">Price: Rs.53990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus 10 pro">
						    <input type="hidden" name="Price" value="53990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoneplus.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="oneplusnord.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus Nord</h6>
						    <p class="card-text">Price: Rs.23990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus Nord">
						    <input type="hidden" name="Price" value="23990">
						    </div>
						</div>
					</form>
				</div>


				
</body>
</html>