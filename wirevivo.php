<?php
   
    include("wiremanagevivo.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="wirevivocart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="wiremanagevivo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="vivo1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">vivo Color Wired in Ear Earphones with Mic and 3.5mm Jack</h6>
						    <p class="card-text">Price: Rs.699</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="vivo Color Wired in Ear Earphones with Mic and 3.5mm Jack">
						    <input type="hidden" name="Price" value="699">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wiremanagevivo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="vivo2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo Extra Bass Original Earphone</h6>
						    <p class="card-text">Price: Rs.132</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo Extra Bass Original Earphone">
						    <input type="hidden" name="Price" value="132">
						    
						    </div>
						</div>
					</form>
				</div>

                <div class="col-lg-3">
					<form action="wiremanagevivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="vivo3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo Wired Earphone</h6>
						    <p class="card-text">Price: Rs.149</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo Wired Earphone">
						    <input type="hidden" name="Price" value="149">
						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wiremanagevivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="vivo4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">VIVO XE160 WIRED</h6>
						    <p class="card-text">Price: Rs.399</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="VIVO XE160 WIRED">
						    <input type="hidden" name="Price" value="399">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wiremanagevivo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="vivo5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo 86+ Extra Bass White Color</h6>
						    <p class="card-text">Price: Rs.199</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo 86+ Extra Bass White Color">
						    <input type="hidden" name="Price" value="199">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wiremanagevivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="vivo6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">VIVO Earphone Headphone Wired Headset</h6>
						    <p class="card-text">Price: Rs.200</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="VIVO Earphone Headphone Wired Headset">
						    <input type="hidden" name="Price" value="200">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wiremanagevivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="vivo7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo og quality earphone</h6>
						    <p class="card-text">Price: Rs.59</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo og quality earphone">
						    <input type="hidden" name="Price" value="59">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="wiremanagevivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="vivo8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">	vivo Color Wired Earphones with Mic and 3.5mm Jack Black</h6>
						    <p class="card-text">Price: Rs.399</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="	vivo Color Wired Earphones with Mic and 3.5mm Jack Black">
						    <input type="hidden" name="Price" value="399">
						    </div>
						</div>
					</form>
				</div>

				

</body>
</html>