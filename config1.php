<?php

// define constant variables
$DB_NAME = "role";
$DB_USER = "root";
$DB_PASSWORD = '';
$DB_HOST = 'localhost';

try{

    // connection variable
    $con = new mysqli_connect($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_NAME);

    // encoded language
    mysqli_set_charset($con, 'utf8');


}catch (Exception $ex){
    print "An Exception occurred. Message: " . $ex->getMessage();
} catch (Error $e){
    print "The system is busy please try later";
}