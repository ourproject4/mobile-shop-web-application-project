<?php
	if (isset($_POST['submit'])){
		// Set connection variables
	    $server = "localhost";
	    $username = "root";
	    $password = "";

	    // Create a database connection
	    $con = mysqli_connect($server, $username, $password);

	    // Check for connection success
	    if(!$con){
	        die("connection to this database failed due to" . mysqli_connect_error());
	    }
	    // echo "Success connecting to the db";

	    // Collect post variables
	    $HardwareId = $_POST['HardwareId'];
	    $rack = $_POST['rack'];
	    $price = $_POST['price'];

	    $sql = "INSERT INTO `role`.`hardwaremodels` (`HardwareId`, `rack`, `price`) VALUES ('$HardwareId', '$rack', '$price');";
	    // echo $sql;

	    // Execute the query
	    if($con->query($sql) == true){
	        // echo "Successfully inserted";
	        header("location:owner.php");

	        // Flag for successful insertion
	        $insert = true;
	    }
	    else{
	        echo "ERROR: $sql <br> $con->error";
	    }

	    // Close the database connection
	    $con->close();
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="style.css">
	<title>Add Hardware Models</title>
</head>
<body>
	 <!-- #Negavation_Bar -->
           <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
              </div>
            </nav>
 <center>
<div class="container">
            <form accept="login.php" method="post">
                <div class="popup-container mt-4">
                    <div class="popup">
                        <div class="col-sm-4">
                            <div class="card">
       
	<center>
		<h4>Add Hardware Models</h4>
       <div class="card-body"> 
		<form action="hardwaremodels.php" method="post">
			<div class="form-group"> 
				<input type="text" class="form-control" name="HardwareId" placeholder="Enter Hardware Id"> <br>
			
				<input type="text" class="form-control" name="rack" placeholder="Enter Rack value"> <br> 
			
				<input type="number" class="form-control" name="price" placeholder="Enter price Of Model"> <br>
			
			<input type="submit" class="btn btn-primary" name="submit" value="Add Model">
		</form>
	</center>
</div>
</div>
</div>
</body>
</html>
