<?php
   
    include("managewireoneplus.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="onepluswirecart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     

				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">

                    
					<form action="managewireoneplus.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oneplus1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">JBL In-Ear Wired JBLT50HIBLUIN Earphone with Mic (Lightweight and Comfortable, Blue)</h6>
						    <p class="card-text">Price: Rs.549</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="JBL In-Ear Wired JBLT50HIBLUIN Earphone with Mic (Lightweight and Comfortable, Blue)">
						    <input type="hidden" name="Price" value="549">						    
						    </div>
						</div>
					</form>
				</div>

                <div class="col-lg-3">
					<form action="managewireoneplus.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oneplus3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">New ONEPLUS Bullets Wireless Z in-Ear Bluetooth Earphones with Mic (Black)</h6>
						    <p class="card-text">Price: Rs.1499</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="New ONEPLUS Bullets Wireless Z in-Ear Bluetooth Earphones with Mic (Black)">
						    <input type="hidden" name="Price" value="1499">
						    
						    </div>
						</div>
					</form>
				</div>

				
                
				<div class="col-lg-3">
					<form action="managewireoneplus.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oneplus4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">4D Oneplus nord Type-C Earphones with Magnetic Buds Wired Headset  (Black, In the Ear)</h6>
						    <p class="card-text">Price: Rs.926</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="4D Oneplus nord Type-C Earphones with Magnetic Buds Wired Headset  (Black, In the Ear)">
						    <input type="hidden" name="Price" value="926">
						    </div>
						</div>
					</form>
				</div>

                     <div class="col-lg-3">
					<form action="managewireoneplus.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oneplus5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus Bullets Earphones (V2) Black</h6>
						    <p class="card-text">Price: Rs.750</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus Bullets Earphones (V2) Black">
						    <input type="hidden" name="Price" value="750">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managewireoneplus.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="oneplus 6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus Bullets Wireless 2 (Black)</h6>
						    <p class="card-text">Price: Rs.799</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus Bullets Wireless 2 (Black)">
						    <input type="hidden" name="Price" value="799">
						    </div>
						</div>
					</form>
				</div>	

				<div class="col-lg-3">
					<form action="managewireoneplus.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oneplus2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus, Stereo Headset, Type C, black, 1692</h6>
						    <p class="card-text">Price: Rs.1299</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus, Stereo Headset, Type C, black, 1692">
						    <input type="hidden" name="Price" value="1299">
						    
						    </div>
						</div>
					</form>
				</div>


					
                

				

</body>
</html>