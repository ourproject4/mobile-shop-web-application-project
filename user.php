<?php

  //#starting_the_sessions
  session_start();

    //if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !==true) {
       // header("location: login.php");
    //}
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title> 
      Home Page
    </title>
     <style>
  .dropbtn {
  background-color: #1a2531;
  color: white;
  padding: 3px;
  font-size: 14px;
  border: none;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: whitesmoke;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: #1a2531;}
.dropdown-toggle{
  color: white;
}
    </style>

  </head>
  <body>

    <!-- #Negavation_Bar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
     
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
      
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="logout.php">Logout</a>
          </li>
          <li class="nav-item active">
        <a class="nav-link" href="login.php">Login</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="newuser.php">Register</a>
      </li>
        </ul>

        <div class="navbar-collapse collapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">

    </ul>
               
            </li>
          </ul>
        </div>
      </div>
      <div class="navbar-collapse collapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <form action="user.php" method="post">
                  <div class="dropdown">
                <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <?php echo "Welcome ". $_SESSION['username']?>  <span class="caret"></span></a></li>
                <div class="dropdown-content">
               <a href="editprofile.php">Edit profile</a>
               <a href="changepass.php">Edit password</a>
               <a href="#">History</a>
               <a href="Hardware.php">Check Avability of Hardware</a>
               <a href="#">Find Hardware</a>
               
  </div>
</div>
            </li>
          </ul>
        </div>
      </div>

    </nav>
    <div class="container mt-4">
      <h3><?php echo "Welcome ". $_SESSION['username']?></h3> <hr>
    </div>
  </body>
</html>
