<!doctype html>
<html lang="en">
  <head>
    <!--#Required_meta_tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--#Bootstrap_CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    


    <title>Home Page</title>
    <style>
    body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}


.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #04AA6D;
  color: white;
}
.btn-primary {
    padding: 5px 20px 5px 20px;
    min-width: 80px;
    background: #D73E0F;
    color: #FFFFFF;
    border-radius: 40px;
    border: none;
    font-size: 12px;
    float: right;
    text-transform: uppercase;
    margin: 15px 12px 15px 0px;
    font-weight: 300;
    position: absolute;
    top: 10px;
    right: 10px;
    letter-spacing: 2px;
    height: 32px;
    
    }

.btn:hover {
  background-color: #3e8e41;
  color: white;

}

 input[type=text] {
  width: 200px;
  box-sizing: border-box;
  border: none;
  border-radius: 20px;
  font-size: 16px;
  float: left;
  position: absolute;
  top: 10px;
  margin-left:700px;
  background-color:white;
  padding: 5px 10px 5px 15px;
  
}

}
.material-icons {
    font-family: 'Material Icons';
    font-weight: normal;
    font-style: normal;
    font-size: 24px;
    line-height: 1;
    letter-spacing: normal;
    text-transform: none;
    display: inline-block;
    white-space: nowrap;
    word-wrap: normal;
    color: white;
    direction: ltr;
    -webkit-font-feature-settings: 'liga';
    -webkit-font-smoothing: antialiased;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown:hover .dropdown-content {
  display: block;
}

.desc {
  padding: 15px;
  text-align: center;
}

.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}

.dropdown:hover .dropbtn {
  background-color: #3e8e41;
}
</style>
  </head>

  <body>
    
  
 <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-header">
      <a class="navbar-brand" href="#">Emera Electronics</a>
    </div>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span> </a>
            </li>

              <a href="newuser.php" style="padding-top:0px;">

   <button type="submit" name="submit" class="btn btn-primary btn-lg"  style="margin: 0px;">
    Sign-In / Register 
     </button>
                </a>
    <form>
            <input type="text" name="search" placeholder="Search..">
          </form>

</nav>
</div>


   <div class="dropdown">
  <img src="mobile.jpg" alt="Cinque Terre" width="210" height="120">
  <div class="dropdown-content">
  <a href="samsung.php">Samsung</a>
  <a href="Vivo.php">Vivo</a>
  <a href="oppo.php">Oppo</a>
  <a href="redmi.php">Redmi</a>
  <a href="Realme.php">Realme</a>
  <a href="oneplus.php">Oneplus</a>
  </div>
</div>

<div class="dropdown">
  <img src="wiredearphone.jpg" alt="Cinque Terre" width="210" height="120">
  <div class="dropdown-content">
  <a href="wiresamsung.php">Samsung</a>
  <a href="wirevivo.php">Vivo</a>
  <a href="wireoppo.php">Oppo</a>
  <a href="wiresony.php">Sony</a>
  <a href="realmeearphone.php">Realme</a>
  <a href="wireboat.php">boat</a>
  <a href="wireoneplus.php">Oneplus</a>
  </div>
</div>

<div class="dropdown">
  <img src="images.jpg" alt="Cinque Terre" width="210" height="120">
  <div class="dropdown-content">
  <a href="headsamsung.php">Samsung</a>
  <a href="vivoheadphone.php">Vivo</a>
  <a href="oppoheadphone.php">Oppo</a>
  <a href="realmeheadphone.php">Realme</a>
  <a href="boatheadphone.php">boat</a>
  <a href="sonyheadphone.php">Sony</a>
  </div>
</div>

<div class="dropdown">
  <img src="buds.jpg" alt="Cinque Terre" width="210" height="120">
  <div class="dropdown-content">
  <a href="samsungbuds.php">Samsung</a>
  <a href="vivobuds.php">Vivo</a>
  <a href="oppobuds.php">Oppo</a>
  <a href="realmebuds.php">Realme</a>
  <a href="boatbuds.php">boat</a>
  <a href="oneplusbuds.php">Oneplus</a>
  </div>
</div>

<div class="dropdown">
  <img src="Bluetooth.png" alt="Cinque Terre" width="210" height="120">
  <div class="dropdown-content">
  <a href="#">Samsung</a>
  <a href="#">Vivo</a>
  <a href="#">Oppo</a>
  <a href="#">Redmi</a>
  <a href="#">Realme</a>
  <a href="#">boat</a>
  <a href="#">Oneplus</a>
  </div>
</div>

<div class="dropdown">
  <img src="charger.jpg" alt="Cinque Terre" width="210" height="120">
  <div class="dropdown-content">
  <a href="#">Samsung</a>
  <a href="#">Vivo</a>
  <a href="#">Oppo</a>
  <a href="#">Redmi</a>
  <a href="#">Realme</a>
  <a href="#">boat</a>
  <a href="#">Oneplus</a>
  </div>
</div> 

  </body>
</html>