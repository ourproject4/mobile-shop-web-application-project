<?php
	if (isset($_POST['submit'])){
		// Set connection variables
	    $server = "localhost";
	    $username = "root";
	    $password = "";

	    // Create a database connection
	    $con = mysqli_connect($server, $username, $password);

	    // Check for connection success
	    if(!$con){
	        die("connection to this database failed due to" . mysqli_connect_error());
	    }
	    // echo "Success connecting to the db";

	    // Collect post variables
	    $HardwareId = $_POST['HardwareId'];
	    $Hardwarename = $_POST['Hardwarename'];
	    $Companyname = $_POST['Companyname'];
	    $Hardwaretype = $_POST['Hardwaretype'];
	    $files = $_FILES['image'];
	    $filename = $files['name'];
	    $fileerror = $files['error'];
	    $filetemp = $files['temp_name'];

	    $fileext = explode('.',$filename);
	    $filecheck = strtolower(end($fileext));

	    $fileextstored = array('png', 'jpg', 'jpeg');

	    if(in_array($filecheck, $fileextstored)) {
	    	$destinationfile = 'HardwarePics/'.$filename;
	    	move_uploaded_file($filetemp, $destinationfile);
	    }

	   	$sql = "INSERT INTO `role`.`hardware`(`HardwareId`, `Hardwarename`, `Companyname`, `Hardwaretype`, `image`) VALUES ('$HardwareId', '$Hardwarename', '$Companyname', '$Hardwaretype', '$destinationfile');";
	   	// echo $sql;

	   	// Execute the query
	    if($con->query($sql) == true){
	        // echo "Successfully inserted";
	        header("location:showHardware.php");

	        // Flag for successful insertion
	        $insert = true;
	    }
	    else{
	        echo "ERROR: $sql <br> $con->error";
	    }

	    // Close the database connection
	    $con->close();
	}
?>

<!DOCTYPE html>
<html>
<head>
	 <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="style.css">
	<title>Add Hardware</title>
	<style>
		  .dropbtn {
		  background-color: #1a2531;
		  color: white;
		  padding: 3px;
		  font-size: 14px;
		  border: none;
		}

		.dropdown {
		  position: relative;
		  display: inline-block;
		}

		.dropdown-content {
		  display: none;
		  position: absolute;
		  background-color: whitesmoke;
		  min-width: 160px;
		  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
		  z-index: 1;
		}

		.dropdown-content a {
		  color: black;
		  padding: 12px 16px;
		  text-decoration: none;
		  display: block;
		}

		.dropdown-content a:hover {background-color: #ddd;}

		.dropdown:hover .dropdown-content {display: block;}

		.dropdown:hover .dropbtn {background-color: #1a2531;}


		.caret.caret-up {
		  border-top-width: 0;
		  color: white;
		}
		.dropdown-toggle{
		  color: white;
		}
	</style>
</head>
<body>
	<!-- #Negavation_Bar -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="#">Emera Electronics</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                </ul>
            </div>
        </nav>
        
	<center>
		<div class="container">
		<form action="hardware.php" method="post" enctype="multipart/form-data">
			<div class="popup-container mt-4">
                    <div class="popup">
                        <div class="col-sm-4">
                            <div class="card">
                                <h5><br>Add Hardware</h5>
                                Fill up the form with correct values
                                <div class="card-body">
                                    <hr class="mb-1"> <br>
				<input type="text" class="form-control" name="hardwarename" placeholder="Enter Hardware Name"> <br>
				<input type="text" class="form-control" name="companyname" placeholder="Enter Company Name"> <br> 
				<input type="text" class="form-control" name="hardwaretype" placeholder="Enter Hardware Type"> <br>
				<input type="file" name="image" class="form-control" placeholder="Select Image"> <br>

			<input type="submit" class="btn btn-primary" name="submit" value="Add Hardware">
		</form>
	</center>
</body>
</html>