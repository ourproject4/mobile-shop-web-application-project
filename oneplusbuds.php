<?php
   
    include("manageoneplusbud.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="oneplusbudcart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="manageoneplusbud.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oneplusbud1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oneplus Buds</h6>
						    <p class="card-text">Price: Rs.1499</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Airdopes 131 RTL In-Ear Truly Wireless Earbuds">
						    <input type="hidden" name="Price" value="1499">						    
						    </div>
						</div>
					</form>
				</div>

				
               

				<div class="col-lg-3">
					<form action="manageoneplusbud.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oneplusbud2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus Buds Z Wireless Headphone, White, (Water and Sweat resistant)</h6>
						    <p class="card-text">Price: Rs.2999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus Buds Z Wireless Headphone, White, (Water and Sweat resistant)">
						    <input type="hidden" name="Price" value="2999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoneplusbud.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="oneplusbud3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus Buds Z Bluetooth Headset </h6>
						    <p class="card-text">Price: Rs.1789</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus Buds Z Bluetooth Headset">
						    <input type="hidden" name="Price" value="1789">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoneplusbud.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oneplusbud4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus Buds Pro True Wireless ANC Bluetooth Earbuds with Mic (Matte Black)</h6> 
						    <p class="card-text">Price: Rs.10190</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus Buds Pro True Wireless ANC Bluetooth Earbuds with Mic (Matte Black)">
						    <input type="hidden" name="Price" value="10190">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoneplusbud.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oneplusbud5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OnePlus Buds Z2 Wireless Earbuds with Dolby Atmos, Obsidian Black</h6>
						    <p class="card-text">Price: Rs.4299</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OnePlus Buds Z2 Wireless Earbuds with Dolby Atmos, Obsidian Black">
						    <input type="hidden" name="Price" value="4299">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoneplusbud.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oneplusbud6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">ONEPLUS BUDS</h6>
						    <p class="card-text">Price: Rs.2947</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="ONEPLUS BUDS">
						    <input type="hidden" name="Price" value="2947">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoneplusbud.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oneplusbud7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">ONE PLUS BUDS PRO BLACK</h6>
						    <p class="card-text">Price: Rs.1399</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="ONE PLUS BUDS PRO BLACK">
						    <input type="hidden" name="Price" value="1399">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoneplusbud.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oneplusbud8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oneplus Buds Pro (E503A) Bluetooth Truly Wireless in Ear Earbuds with mic</h6>
						    <p class="card-text">Price: Rs.8699</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oneplus Buds Pro (E503A) Bluetooth Truly Wireless in Ear Earbuds with mic">
						    <input type="hidden" name="Price" value="8699">
						    </div>
						</div>
					</form>
				</div>
                
                <div class="col-lg-3">
					<form action="manageoneplusbud.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oneplusbud9.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Case For Oneplus Buds Pro, Transparent Tpu Case With Keychain Waterproof </h6>
						    <p class="card-text">Price: Rs.1155</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Case For Oneplus Buds Pro, Transparent Tpu Case With Keychain Waterproof ">
						    <input type="hidden" name="Price" value="1155">
						    
						    </div>
						</div>
					</form>
				</div>

</body>
</html>