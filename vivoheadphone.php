<?php
   
    include("manageheadvivo.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="vivoheadcart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="manageheadvivo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="headvivo1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo Headphones [ Extra Base Stereo Headphones]</h6>
						    <p class="card-text">Price: Rs.760</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo Headphones [ Extra Base Stereo Headphones]">
						    <input type="hidden" name="Price" value="760">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageheadvivo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="headvivo2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo MDR-XB450AP On Ear Wired With Mic Headphones/Earphones</h6>
						    <p class="card-text">Price: Rs.599</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo MDR-XB450AP On Ear Wired With Mic Headphones/Earphones">
						    <input type="hidden" name="Price" value="599">
						    
						    </div>
						</div>
					</form>
				</div>

               

				<div class="col-lg-3">
					<form action="manageheadvivo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="headvivo3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">VIVO Headphones XE1000 White Brand New Original</h6>
						    <p class="card-text">Price: Rs.1550</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="VIVO Headphones XE1000 White Brand New Original">
						    <input type="hidden" name="Price" value="1550">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageheadvivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="headvivo4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">SPI. Vivo X3S compatible Over Ear Wireless With Mic Headphones/Earphones</h6>
						    <p class="card-text">Price: Rs.1299</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="SPI. Vivo X3S compatible Over Ear Wireless With Mic Headphones/Earphones">
						    <input type="hidden" name="Price" value="1299">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageheadvivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="headvivo5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">SPI. Vivo X3S compatible Over Ear Wireless With Mic Headphones/Earphones</h6> <p class="card-text">Price: Rs.9999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="SPI. Vivo X3S compatible Over Ear Wireless With Mic Headphones/Earphones">
						    <input type="hidden" name="Price" value="9999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageheadvivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="headvivo6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Zebronics Vivo Bluetooth Headphone (Black, Over the Ear)</h6>
						    <p class="card-text">Price: Rs.890</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Zebronics Vivo Bluetooth Headphone (Black, Over the Ear)">
						    <input type="hidden" name="Price" value="890">
						    </div>
						</div>
					</form>
				</div>
                
                <div class="col-lg-3">
					<form action="manageheadvivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="headvivo7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">V7 Premium 3.5 Mm On-ear Headphones</h6>
						    <p class="card-text">Price: Rs.1118</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="V7 Premium 3.5 Mm On-ear Headphones">
						    <input type="hidden" name="Price" value="1118">
						    
						    </div>
						</div>
					</form>
				</div>


				<div class="col-lg-3">
					<form action="manageheadvivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="headvivo8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">V7 Deluxe 3.5 Mm Stereo Headphones</h6>
						    <p class="card-text">Price: Rs.1082</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="V7 Deluxe 3.5 Mm Stereo Headphones">
						    <input type="hidden" name="Price" value="1082">
						    </div>
						</div>
					</form>
				</div>

				
</body>
</html>