<?php
   
    include("managesamsung.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="samsungwirecart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="managesamsung.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="samsungEHS64 In-Ear wired.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung EHS64AVFBECINU In-Ear Wired Earphones with Mic (Black)</h6>
						    <p class="card-text">Price: Rs.399</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung EHS64AVFBECINU In-Ear Wired Earphones with Mic (Black)">
						    <input type="hidden" name="Price" value="399">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesamsung.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="samsung3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Earphone for Samsung Galaxy J2 Prime Universal Wired Stereo Bass Head </h6>
						    <p class="card-text">Price: Rs.399</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Earphone for Samsung Galaxy J2 Prime Universal Wired Stereo Bass Head ">
						    <input type="hidden" name="Price" value="399">
						    
						    </div>
						</div>
					</form>
				</div>

                <div class="col-lg-3">
					<form action="managesamsung.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="samsung2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title"> Samsung EHS64AVFWE In-Ear Wired Earphones with Mic (White)</h6>
						    <p class="card-text">Price: Rs.499</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung EHS64AVFWE In-Ear Wired Earphones with Mic (White)">
						    <input type="hidden" name="Price" value="499">
						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesamsung.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="samsung4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung HF-34 Wired In the EarType</h6>
						    <p class="card-text">Price: Rs.108</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung HF-34 Wired In the EarType">
						    <input type="hidden" name="Price" value="108">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesamsung.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="samsung5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung j8 earphone samsung original earphone samsung headphones bass earphone samsung earbuds</h6>
						    <p class="card-text">Price: Rs.126</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung j8 earphone samsung original earphone samsung headphones bass earphone samsung earbuds">
						    <input type="hidden" name="Price" value="126">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesamsung.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="samsung6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">SAMSUNG GALAXY 3.5mm Wired Headphones Half In-Ear Earbuds</h6>
						    <p class="card-text">Price: Rs.1714</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="SAMSUNG GALAXY 3.5mm Wired Headphones Half In-Ear Earbuds">
						    <input type="hidden" name="Price" value="1714">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesamsung.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="samsung7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Wired in Ear Headphones with Mic -Pink </h6>
						    <p class="card-text">Price: Rs.139</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Wired in Ear Headphones with Mic -Pink ">
						    <input type="hidden" name="Price" value="139">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesamsung.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="samsung8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Earphone EG920 Blue Arctic</h6>
						    <p class="card-text">Price: Rs.950</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Earphone EG920 Blue Arctic">
						    <input type="hidden" name="Price" value="950">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesamsung.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="samsung9.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">	Samsung EHS64 Earphone (White)</h6>
						    <p class="card-text">Price: Rs.419</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung EHS64 Earphone (White)">
						    <input type="hidden" name="Price" value="419">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesamsung.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="samsung10.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">	Samsung Tuned by AKG Type C Earphones - Black (Bulk)</h6>
						    <p class="card-text">Price: Rs.1499</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Tuned by AKG Type C Earphones - Black (Bulk)">
						    <input type="hidden" name="Price" value="1499">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesamsung.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="samsung11.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung EHS64 In-Ear Headphones Black</h6>
						    <p class="card-text">Price: Rs.499</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung EHS64 In-Ear Headphones Black">
						    <input type="hidden" name="Price" value="499">
						    </div>
						</div>
					</form>
				</div>


</body>
</html>