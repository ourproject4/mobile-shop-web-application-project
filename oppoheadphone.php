<?php
   
    include("oppomanage.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="oppoheadcart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="oppomanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppohead1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Alishark Best Sh-12 Bluetooth Headphone A7 For Oppo/Vivo/Samsng/Nokia Bluetooth ...</h6>
						    <p class="card-text">Price: Rs.760</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Alishark Best Sh-12 Bluetooth Headphone A7 For Oppo/Vivo/Samsng/Nokia Bluetooth ...">
						    <input type="hidden" name="Price" value="760">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="oppomanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppohead2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">bluetooth headset for oppo,2 Pieces</h6>
						    <p class="card-text">Price: Rs.3844</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="bluetooth headset for oppo,2 Pieces">
						    <input type="hidden" name="Price" value="3844">
						    
						    </div>
						</div>
					</form>
				</div>

               

				<div class="col-lg-3">
					<form action="oppomanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppohead3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">WILES SH 12 OPPO,VIVO,MI COMPATIBLE ALL Over Ear Wireless With Mic Headphones/Earphones</h6>
						    <p class="card-text">Price: Rs.1089</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="WILES SH 12 OPPO,VIVO,MI COMPATIBLE ALL Over Ear Wireless With Mic Headphones/Earphones">
						    <input type="hidden" name="Price" value="1089">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="oppomanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="oppohead4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">hitage Stereo Headphones Bluetooth Headset Over Ear Wireless With Mic Headphones/Earphones Gray</h6>
						    <p class="card-text">Price: Rs.899</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="hitage Stereo Headphones Bluetooth Headset Over Ear Wireless With Mic Headphones/Earphones Gray">
						    <input type="hidden" name="Price" value="899">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="oppomanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oppohead5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">WILES SH 12 OPPO,VIVO,MI COMPATIBLE ALL Over Ear Wireless With Mic Headphones/Earphones</h6> <p class="card-text">Price: Rs.999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="WILES SH 12 OPPO,VIVO,MI COMPATIBLE ALL Over Ear Wireless With Mic Headphones/Earphones">
						    <input type="hidden" name="Price" value="999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="oppomanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="oppohead6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oud OD-HP-D-08B Bluetooth Headphones With Inbuilt Mic</h6>
						    <p class="card-text">Price: Rs.1089</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oud OD-HP-D-08B Bluetooth Headphones With Inbuilt Mic">
						    <input type="hidden" name="Price" value="1089">
						    </div>
						</div>
					</form>
				</div>
                
                
</body>
</html>