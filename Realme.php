<?php
   
    include("manageRealme.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="realmecart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="manageRealme.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realme6i64.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme6i 64 GB</h6>
						    <p class="card-text">Price: Rs.13999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme6i 64 GB">
						    <input type="hidden" name="Price" value="13999">						    
						    </div>
						</div>
					</form>
				</div>


				<div class="col-lg-3">
					<form action="manageRealme.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="realme7pro.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme 7 pro</h6>
						    <p class="card-text">Price: Rs.22599</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme 7 pro">
						    <input type="hidden" name="Price" value="22599">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRealme.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realme8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme 8</h6>
						    <p class="card-text">Price: Rs.5990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme 8">
						    <input type="hidden" name="Price" value="5990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRealme.php" method="post">
						<div class="card" style="width: 10rem; " >
						  <img src="realme8i.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme 8i</h6>
						    <p class="card-text">Price: Rs.13990</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme 8i">
						    <input type="hidden" name="Price" value="13990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRealme.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="realme9pro.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme 9 pro</h6>
						    <p class="card-text">Price: Rs.20990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme 9 pro">
						    <input type="hidden" name="Price" value="20990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRealme.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="realme78gb.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme7 8GB</h6>
						    <p class="card-text">Price: Rs.16990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme7 8GB">
						    <input type="hidden" name="Price" value="16990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRealme.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realmeC21Y.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme C21Y</h6>
						    <p class="card-text">Price: Rs.10490</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme C21Y">
						    <input type="hidden" name="Price" value="10490">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRealme.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realmeC25.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme C25</h6>
						    <p class="card-text">Price: Rs.11990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme C25">
						    <input type="hidden" name="Price" value="11990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRealme.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realmeC35.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme C35</h6>
						    <p class="card-text">Price: Rs.12990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme C35">
						    <input type="hidden" name="Price" value="12990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRealme.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="realmeGTNeo2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme GT Neo2</h6>
						    <p class="card-text">Price: Rs.31990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme GT Neo2">
						    <input type="hidden" name="Price" value="31990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRealme.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="realmeNarzo.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme Narzo</h6>
						    <p class="card-text">Price: Rs.14890</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme Narzo">
						    <input type="hidden" name="Price" value="14890">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRealme.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="realmeX.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme X</h6>
						    <p class="card-text">Price: Rs.17990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme X">
						    <input type="hidden" name="Price" value="17990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRealme.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="realmeXT.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme XT</h6>
						    <p class="card-text">Price: Rs.16000</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme XT">
						    <input type="hidden" name="Price" value="16000">
						    </div>
						</div>
					</form>
				</div>


                <div class="col-lg-3">
					<form action="manageRealme.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="realme7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme 7</h6>
						    <p class="card-text">Price: Rs.6999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme 7">
						    <input type="hidden" name="Price" value="6999">
						    
						    </div>
						</div>
					</form>
				</div>



</body>
</html>