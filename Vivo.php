<?php
   
    include("manageVivo.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="VivoCart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="manageVivo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="Vivo T1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo T1</h6>
						    <p class="card-text">Price: Rs.22990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo T1">
						    <input type="hidden" name="Price" value="22990">						    
						    </div>
						</div>
					</form>
				</div>


                <div class="col-lg-3">
					<form action="manageVivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="Vivo V15pro.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo V15 Pro Smartphone</h6>
						    <p class="card-text">Price: Rs.12200</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo V15 Pro Smartphone">
						    <input type="hidden" name="Price" value="12200">
						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageVivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="Vivo V17Pro.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo V17Pro</h6>
						    <p class="card-text">Price: Rs.12499</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo V17Pro">
						    <input type="hidden" name="Price" value="12499">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageVivo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="Vivo V19Neo.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo V19 Neo</h6>
						    <p class="card-text">Price: Rs.16500</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo V19 Neo">
						    <input type="hidden" name="Price" value="16500">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageVivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="Vivo V21.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo V21</h6>
						    <p class="card-text">Price: Rs.24990</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo V21">
						    <input type="hidden" name="Price" value="24990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageVivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="Vivo V23 E.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo V23 e </h6>
						    <p class="card-text">Price: Rs.29990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo V23 e">
						    <input type="hidden" name="Price" value="29990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageVivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="Vivo V23.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo V23</h6>
						    <p class="card-text">Price: Rs.29990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo V23">
						    <input type="hidden" name="Price" value="29990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageVivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="Vivo X70Pro.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo X70Pro</h6>
						    <p class="card-text">Price: Rs.79990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo X70Pro">
						    <input type="hidden" name="Price" value="79990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageVivo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="Vivo Y73.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo Y73</h6>
						    <p class="card-text">Price: Rs.20200</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo Y73">
						    <input type="hidden" name="Price" value="20200">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageVivo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="Vivo Z1 Pro.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo Z1 Pro</h6>
						    <p class="card-text">Price: Rs.8990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo Z1 Pro">
						    <input type="hidden" name="Price" value="8990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageVivo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="VivoV21.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Vivo V21</h6>
						    <p class="card-text">Price: Rs.24990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Vivo V21">
						    <input type="hidden" name="Price" value="24990">
						    </div>
						</div>
					</form>
				</div>


</body>
</html>