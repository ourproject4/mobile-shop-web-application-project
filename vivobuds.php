<?php
   
    include("managebudsvivo.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="vivobudscart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="managebudsvivo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="vivobuds1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">vivo TWS NEO Earphones</h6>
						    <p class="card-text">Price: Rs.5990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="vivo TWS NEO Earphones">
						    <input type="hidden" name="Price" value="5990">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managebudsvivo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="vivobuds2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Original vivo TWS 2e 6020103 Bluetooth 5.2 True Wireless Bluetooth Earphone(White)</h6>
						    <p class="card-text">Price: Rs.6690</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Original vivo TWS 2e 6020103 Bluetooth 5.2 True Wireless Bluetooth Earphone(White)">
						    <input type="hidden" name="Price" value="6690">
						    
						    </div>
						</div>
					</form>
				</div>

               

				<div class="col-lg-3">
					<form action="managebudsvivo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="vivobuds3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Croma CREEH1901sBTEB In-Ear Truly Wireless Earbuds with Mic (Bluetooth 5.1, 5 Hours Play Time, White)</h6>
						    <p class="card-text">Price: Rs.1599</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Croma CREEH1901sBTEB In-Ear Truly Wireless Earbuds with Mic (Bluetooth 5.1, 5 Hours Play Time, White)">
						    <input type="hidden" name="Price" value="1599">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managebudsvivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="vivobuds4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Air Buds Pro</h6>
						    <p class="card-text">Price: Rs.2499</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Air Buds Pro">
						    <input type="hidden" name="Price" value="2499">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managebudsvivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="vivobuds5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">The Sound Expert XPODS</h6> 
						    <p class="card-text">Price: Rs.1299</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="The Sound Expert XPODS">
						    <input type="hidden" name="Price" value="1299">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managebudsvivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="vivobuds6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Noise Buds Prima AUD-HDPHN-BUDSPRIM In-Ear Truly Wireless Earbuds with Mic</h6>
						    <p class="card-text">Price: Rs.1990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Noise Buds Prima AUD-HDPHN-BUDSPRIM In-Ear Truly Wireless Earbuds with Mic">
						    <input type="hidden" name="Price" value="1990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managebudsvivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="vivobuds7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Buds VS103</h6>
						    <p class="card-text">Price: Rs.1999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Buds VS103">
						    <input type="hidden" name="Price" value="1999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managebudsvivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="vivobuds8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Buds VS303</h6>
						    <p class="card-text">Price: Rs.1799</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Buds VS303">
						    <input type="hidden" name="Price" value="1799">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managebudsvivo.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="vivobuds9.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">DIZO Buds Z (by realme TechLife) (True Wirless, Onyx)</h6>
						    <p class="card-text">Price: Rs.7149</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="DIZO Buds Z (by realme TechLife) (True Wirless, Onyx)">
						    <input type="hidden" name="Price" value="7149">
						    </div>
						</div>
					</form>
				</div>
                
                
</body>
</html>