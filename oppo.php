<?php
   
    include("manageoppo.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="oppocart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="manageoppo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppoA3s.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo A3s</h6>
						    <p class="card-text">Price: Rs.9990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo A3s">
						    <input type="hidden" name="Price" value="9990">						    
						    </div>
						</div>
					</form>
				</div>


                <div class="col-lg-3">
					<form action="manageoppo.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="oppoA5s.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo A5s</h6>
						    <p class="card-text">Price: Rs.9490</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo A5s">
						    <input type="hidden" name="Price" value="9490">
						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoppo.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="oppoA11k.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo A11k</h6>
						    <p class="card-text">Price: Rs.8990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo A11k">
						    <input type="hidden" name="Price" value="8990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoppo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppoA12.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo A12</h6>
						    <p class="card-text">Price: Rs.9990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo A12">
						    <input type="hidden" name="Price" value="9990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoppo.php" method="post">
						<div class="card" style="width: 10rem; " >
						  <img src="oppoA16k.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo A16k</h6>
						    <p class="card-text">Price: Rs.9990</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo A16k">
						    <input type="hidden" name="Price" value="9990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoppo.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="oppoF17Pro.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo F17 Pro</h6>
						    <p class="card-text">Price: Rs.19990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo F17 Pro">
						    <input type="hidden" name="Price" value="19990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoppo.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="oppoA53s.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo A53s</h6>
						    <p class="card-text">Price: Rs.17990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo A53s">
						    <input type="hidden" name="Price" value="17990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoppo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppo A54.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo A54</h6>
						    <p class="card-text">Price: Rs.15990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo A54">
						    <input type="hidden" name="Price" value="15990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoppo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppoF17.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo F17</h6>
						    <p class="card-text">Price: Rs.20990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo F17">
						    <input type="hidden" name="Price" value="20990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoppo.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="oppoF19Pro.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Oppo F19 Pro</h6>
						    <p class="card-text">Price: Rs.21990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Oppo F19 Pro">
						    <input type="hidden" name="Price" value="21990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageoppo.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="oppoReno7Pro5G.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">OppoReno 7Pro 5G</h6>
						    <p class="card-text">Price: Rs.39990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="OppoReno 7Pro 5G">
						    <input type="hidden" name="Price" value="39990">
						    </div>
						</div>
					</form>
				</div>


</body>
</html>