<?php
  //#Starting_the_session_of_php
  session_start();

  //#Creating_the_connection
  $conn = mysqli_connect("localhost","root","","role");

  //#query
  $query = mysqli_query($conn,"select * from login where username='".$_SESSION['username']."'");
  
  //#Logic : Dividing_by_roles
  while ($row=mysqli_fetch_array($query)) {
    $role=$row['role'];
  }

  //#Goes_to_owner_page
  if ($role=="owner") {
    header("location:owner.php");
  }

  //#Goes_to_manager_page
  elseif ($role=="manager") { 
    header("location:manager.php");
  }

  //#Goes_to_user_page
  elseif($role=="user") {  
    header("location:user.php");
  }
?>
