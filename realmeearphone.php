<?php
   
    include("managerealmeear.php");
    include("purchase.php")
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="realmeearCart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="managerealmeear.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realme 6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">realme Orange Buds 3</h6>
						    <p class="card-text">Price: Rs.999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="realme Orange Buds 3">
						    <input type="hidden" name="Price" value="999">						    
						    </div>
						</div>
					</form>
				</div>


				<div class="col-lg-3">
					<form action="managerealmeear.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="realme 10.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme Buds 2 Wired Earphones phone</h6>
						    <p class="card-text">Price: Rs.1999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme Buds 2 Wired Earphones phone">
						    <input type="hidden" name="Price" value="1999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managerealmeear.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realme 9.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme Buds Classic Wired Earphones</h6>
						    <p class="card-text">Price: Rs.3990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme Buds Classic Wired Earphones">
						    <input type="hidden" name="Price" value="3990">
						    </div>
						</div>
					</form>
				</div>


				<div class="col-lg-3">
					<form action="managerealmeear.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="realme 8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme Buds Classic Earphones(Black)</h6>
						    <p class="card-text">Price: Rs.990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme Buds Classic Earphones(Black)">
						    <input type="hidden" name="Price" value="990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managerealmeear.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="realme 7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">realme Buds 2 Neo Wired in Ear Earphones</h6>
						    <p class="card-text">Price: Rs.6990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="realme Buds 2 Neo Wired in Ear Earphones">
						    <input type="hidden" name="Price" value="6990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managerealmeear.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realme 5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">realme Buds 2 Wired in Ear Earphones(Green)</h6>
						    <p class="card-text">Price: Rs.2999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="realme Buds 2 Wired in Ear Earphones(Green)">
						    <input type="hidden" name="Price" value="2999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managerealmeear.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realme 4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">realme Buds 2 Neo Wired in Ear Earphones</h6>
						    <p class="card-text">Price: Rs.3990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="realme Buds 2 Neo Wired in Ear Earphones">
						    <input type="hidden" name="Price" value="3990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managerealmeear.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="realme 2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Realme Buds Classic Wired Earphones</h6>
						    <p class="card-text">Price: Rs.7990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Realme Buds Classic Wired Earphones">
						    <input type="hidden" name="Price" value="7990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managerealmeear.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="realme 3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">realme Buds Classic Wired in Ear Earphones</h6>
						    <p class="card-text">Price: Rs.2990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="realme Buds Classic Wired in Ear Earphones">
						    <input type="hidden" name="Price" value="2990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managerealmeear.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="realme 1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">realme Buds 2 Wired in Ear Earphones</h6>
						    <p class="card-text">Price: Rs.3990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="realme Buds 2 Wired in Ear Earphones">
						    <input type="hidden" name="Price" value="3990">
						    </div>
						</div>
					</form>
				</div>

				
</body>
</html>