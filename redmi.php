<?php
   
    include("manageRedmi.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="redmicart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="manageRedmi.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="xiaomiredmik20.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Xiaomi Redmi K20</h6>
						    <p class="card-text">Price: Rs.19990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Xiaomi Redmi K20">
						    <input type="hidden" name="Price" value="19990">						    
						    </div>
						</div>
					</form>
				</div>


                <div class="col-lg-3">
					<form action="manageRedmi.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="redmi 10.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Redmi 10</h6>
						    <p class="card-text">Price: Rs.9490</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Redmi 10">
						    <input type="hidden" name="Price" value="9490">
						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRedmi.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="redmi 7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Redmi 7</h6>
						    <p class="card-text">Price: Rs.8990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Redmi 7">
						    <input type="hidden" name="Price" value="8990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRedmi.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="redminote8pro.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Redmi Note 8 Pro</h6>
						    <p class="card-text">Price: Rs.9990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Redmi Note 8 Pro">
						    <input type="hidden" name="Price" value="9990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRedmi.php" method="post">
						<div class="card" style="width: 10rem; " >
						  <img src="redminote 11.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">RedmiNote 11</h6>
						    <p class="card-text">Price: Rs.11990</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="RedmiNote 11">
						    <input type="hidden" name="Price" value="11990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRedmi.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="redminote11S.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Redmi Note 11S</h6>
						    <p class="card-text">Price: Rs.19990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Redmi Note 11S">
						    <input type="hidden" name="Price" value="19990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRedmi.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="RedmiNote11.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">RedmiNote 11</h6>
						    <p class="card-text">Price: Rs.17990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="RedmiNote 11">
						    <input type="hidden" name="Price" value="17990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRedmi.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="Redmi9A.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Redmi 9A</h6>
						    <p class="card-text">Price: Rs.16990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Redmi 9A">
						    <input type="hidden" name="Price" value="16990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRedmi.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="Redmi10.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Redmi 10 prime</h6>
						    <p class="card-text">Price: Rs.10990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Redmi 10 prime">
						    <input type="hidden" name="Price" value="10990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRedmi.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="Redminote10T.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Redmi Note 10T</h6>
						    <p class="card-text">Price: Rs.21990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Redmi Note 10T">
						    <input type="hidden" name="Price" value="21990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageRedmi.php" method="post">
						<div class="card" style="width: 10rem; ">
						  <img src="Redmi 9.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Redmi 9</h6>
						    <p class="card-text">Price: Rs.9990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Redmi 9">
						    <input type="hidden" name="Price" value="9990">
						    </div>
						</div>
					</form>
				</div>


</body>
</html>