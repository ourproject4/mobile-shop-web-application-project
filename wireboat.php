<?php
   
    include("manageboat.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="boatcart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="manageboat.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="boat1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt BassHeads 100 In-Ear Wired Earphones With Super Extra Bass</h6>
						    <p class="card-text">Price: Rs.299</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt BassHeads 100 In-Ear Wired Earphones With Super Extra Bass">
						    <input type="hidden" name="Price" value="299">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageboat.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="boat2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt Bassheads 242 in Ear Wired Earphones with Mic(Active Black) </h6>
						    <p class="card-text">Price: Rs.499</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Bassheads 242 in Ear Wired Earphones with Mic(Active Black)">
						    <input type="hidden" name="Price" value="499">
						    
						    </div>
						</div>
					</form>
				</div>

                <div class="col-lg-3">
					<form action="manageboat.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="boat3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title"> boAt Bassheads 162 in Ear Wired Earphones with Mic(Active Black)</h6>
						    <p class="card-text">Price: Rs.250</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Bassheads 162 in Ear Wired Earphones with Mic(Active Black)">
						    <input type="hidden" name="Price" value="250">
						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageboat.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="boat4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">BOAT BassHeads 220</h6>
						    <p class="card-text">Price: Rs.499</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="BOAT BassHeads 220">
						    <input type="hidden" name="Price" value="499">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageboat.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="boat5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt BassHeads 103</h6>
						    <p class="card-text">Price: Rs.350</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt BassHeads 103">
						    <input type="hidden" name="Price" value="350">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageboat.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="boat6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt BassHeads 100 In-Ear Wired Earphones With Super Extra Bass</h6>
						    <p class="card-text">Price: Rs.299</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt BassHeads 100 In-Ear Wired Earphones With Super Extra Bass">
						    <input type="hidden" name="Price" value="299">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageboat.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="boat7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt BassHeads 103 In Ear Wired Headphone With Mic</h6>
						    <p class="card-text">Price: Rs.349</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt BassHeads 103 In Ear Wired Headphone With Mic">
						    <input type="hidden" name="Price" value="349">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageboat.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="boat8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt Bassheads 105 Wired in Ear Earphones with Mic (White)</h6>
						    <p class="card-text">Price: Rs.399</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Bassheads 105 Wired in Ear Earphones with Mic (White)">
						    <input type="hidden" name="Price" value="399">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageboat.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="boat9.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">	boAt BassHeads 102 Wired Earphones with Immersive Audio (Black)</h6>
						    <p class="card-text">Price: Rs.499</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt BassHeads 102 Wired Earphones with Immersive Audio (Black)">
						    <input type="hidden" name="Price" value="499">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageboat.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="boat10.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">	boAt BassHeads 242 Wired Earphones (Lime)</h6>
						    <p class="card-text">Price: Rs.550</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt BassHeads 242 Wired Earphones (Lime)">
						    <input type="hidden" name="Price" value="550">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageboat.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="boat11.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt Handsfree Metal Earphone (BASSHEADS 229)</h6>
						    <p class="card-text">Price: Rs.259</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Handsfree Metal Earphone (BASSHEADS 229)">
						    <input type="hidden" name="Price" value="259">
						    </div>
						</div>
					</form>
				</div>


</body>
</html>