<?php
   
    include("managesony.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="sonycart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
               
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="managesony.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="sony1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony MDR-EX155AP Wired In-Ear Headphones</h6>
						    <p class="card-text">Price: Rs.1161</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony MDR-EX155AP Wired In-Ear Headphones">
						    <input type="hidden" name="Price" value="1161">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesony.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="sony2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">SONY MDR-E9LP IN-EAR HEADPHONES </h6>
						    <p class="card-text">Price: Rs.349</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="SONY MDR-E9LP IN-EAR HEADPHONES ">
						    <input type="hidden" name="Price" value="349">
						    
						    </div>
						</div>
					</form>
				</div>

                <div class="col-lg-3">
					<form action="managesony.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="sony3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony MDR-EX14AP Wired In-Ear Headphones </h6>
						    <p class="card-text">Price: Rs.776</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony MDR-EX14AP Wired In-Ear Headphones">
						    <input type="hidden" name="Price" value="776">
						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesony.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="sony4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony MDR-AS210 Wired Open-Ear Headphones </h6>
						    <p class="card-text">Price: Rs.801</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony MDR-AS210 Wired Open-Ear Headphones ">
						    <input type="hidden" name="Price" value="801">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesony.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="sony5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony MDR-XB55AP in-Ear Extra Bass Headphones </h6>
						    <p class="card-text">Price: Rs.1599</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony MDR-XB55AP in-Ear Extra Bass Headphones ">
						    <input type="hidden" name="Price" value="1599">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesony.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="sony6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">	SONY MDR-EX150/B </h6>
						    <p class="card-text">Price: Rs.499</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="	SONY MDR-EX150/B ">
						    <input type="hidden" name="Price" value="499">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesony.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="sony7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony Ericsson HPM-70 Stereo Headphones</h6>
						    <p class="card-text">Price: Rs.1169</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony Ericsson HPM-70 Stereo Headphones">
						    <input type="hidden" name="Price" value="1169">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesony.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="sony8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">SONY MDR-XB510AS WIRED SPORTS IN-EAR</h6>
						    <p class="card-text">Price: Rs.1799</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="SONY MDR-XB510AS WIRED SPORTS IN-EAR">
						    <input type="hidden" name="Price" value="1799">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesony.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="sony9.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony MDR-E9LP White</h6>
						    <p class="card-text">Price: Rs.799</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony MDR-E9LP White">
						    <input type="hidden" name="Price" value="799">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesony.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="sony10.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony MDR-AS210 Earphone</h6>
						    <p class="card-text">Price: Rs.799</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony MDR-AS210 Earphone">
						    <input type="hidden" name="Price" value="799">
						    </div>
						</div>
					</form>
				</div>

				
				<div class="col-lg-3">
					<form action="managesony.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="sony12.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony Digital Noise Cancelling Headset MDR-NC31E</h6>
						    <p class="card-text">Price: Rs.2145</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony Digital Noise Cancelling Headset MDR-NC31E">
						    <input type="hidden" name="Price" value="2145">
						    </div>
						</div>
					</form>
				</div>


</body>
</html>