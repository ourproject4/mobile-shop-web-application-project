<?php
   
    include("boatmanage.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="boatheadcart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="boatmanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="boat 1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Boat Rockerz 558 Over-Ear Wireless Headphone With Mic (Bluetooth 5.0, Physical Noise Isolation, Black)</h6>
						    <p class="card-text">Price: Rs.1399</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Boat Rockerz 558 Over-Ear Wireless Headphone With Mic (Bluetooth 5.0, Physical Noise Isolation, Black)">
						    <input type="hidden" name="Price" value="1399">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="boatmanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="boat 2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">BOAT BassHeads 900</h6>
						    <p class="card-text">Price: Rs.850</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="BOAT BassHeads 900">
						    <input type="hidden" name="Price" value="850">
						    
						    </div>
						</div>
					</form>
				</div>

               

				<div class="col-lg-3">
					<form action="boatmanage.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="boat 3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt Rockerz 450R On-Ear Wireless Headphone with Mic (Bluetooth 4.2, Luscious Black)</h6>
						    <p class="card-text">Price: Rs.1080</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Rockerz 450R On-Ear Wireless Headphone with Mic (Bluetooth 4.2, Luscious Black)">
						    <input type="hidden" name="Price" value="1080">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="boatmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="boat 4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Trebel Rockerz 550</h6>
						    <p class="card-text">Price: Rs.1099</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Trebel Rockerz 550">
						    <input type="hidden" name="Price" value="1099">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="boatmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="boat 5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt Rockerz 660</h6> 
						    <p class="card-text">Price: Rs.1750</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Rockerz 660">
						    <input type="hidden" name="Price" value="1750">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="boatmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="boat 6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt Immortal 400</h6>
						    <p class="card-text">Price: Rs.1999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Immortal 400">
						    <input type="hidden" name="Price" value="1999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="boatmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="boat 7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">boAt Immortal 1000D</h6>
						    <p class="card-text">Price: Rs.2499</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="boAt Immortal 1000D">
						    <input type="hidden" name="Price" value="2499">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="boatmanage.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="boat 8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">BOAT BassHeads 900</h6>
						    <p class="card-text">Price: Rs.1699</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="BOAT BassHeads 900">
						    <input type="hidden" name="Price" value="1699">
						    </div>
						</div>
					</form>
				</div>
                
                
</body>
</html>