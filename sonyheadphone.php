<?php
   
    include("managesonyhead.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="sonycarthead.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="managesonyhead.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="sonyhead1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony WH-CH510 Wireless Headphones</h6>
						    <p class="card-text">Price: Rs.2790</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony WH-CH510 Wireless Headphones">
						    <input type="hidden" name="Price" value="2790">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesonyhead.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="sonyhead2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony MDR-XB450AP Wired Headphone with Mic, Extra Bass, Tangle-free cable (Red)</h6>
						    <p class="card-text">Price: Rs.2490</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony MDR-XB450AP Wired Headphone with Mic, Extra Bass, Tangle-free cable (Red)">
						    <input type="hidden" name="Price" value="2490">
						    
						    </div>
						</div>
					</form>
				</div>

               

				<div class="col-lg-3">
					<form action="managesonyhead.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="sonyhead3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony MDR-ZX110AP Wired On-Ear Headphones with tangle free cable</h6>
						    <p class="card-text">Price: Rs.1080</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony MDR-ZX110AP Wired On-Ear Headphones with tangle free cable">
						    <input type="hidden" name="Price" value="1080">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesonyhead.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="sonyhead4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony WH-CH710N Noise Cancelling Wireless Headphones</h6>
						    <p class="card-text">Price: Rs.8099</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony WH-CH710N Noise Cancelling Wireless Headphones">
						    <input type="hidden" name="Price" value="8099">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesonyhead.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="sonyhead5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony MDR-XB450AP Wired Headphone with Mic, Extra Bass, Tangle-free cable (Black)</h6> 
						    <p class="card-text">Price: Rs.2199</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony MDR-XB450AP Wired Headphone with Mic, Extra Bass, Tangle-free cable (Black)">
						    <input type="hidden" name="Price" value="2199">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesonyhead.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="sonyhead6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony WH-CH510 Wireless Headphones - Black</h6>
						    <p class="card-text">Price: Rs.2990</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony WH-CH510 Wireless Headphones - Black">
						    <input type="hidden" name="Price" value="2990">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesonyhead.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="sonyhead7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony MDR-ZX110A On-Ear Stereo Headphones (White)</h6>
						    <p class="card-text">Price: Rs.499</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony MDR-ZX110A On-Ear Stereo Headphones (White)">
						    <input type="hidden" name="Price" value="499">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="managesonyhead.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="sonyhead8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Sony WH-1000XM3 Wireless Noise-Canceling Over-Ear Headphones (Silver)</h6>
						    <p class="card-text">Price: Rs.17999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Sony WH-1000XM3 Wireless Noise-Canceling Over-Ear Headphones (Silver)">
						    <input type="hidden" name="Price" value="17999">
						    </div>
						</div>
					</form>
				</div>
                
                
</body>
</html>