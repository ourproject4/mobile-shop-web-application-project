<?php
   
    include("manageheadsamsung.php");
    include("purchase.php");
?>

<html>
<head>
	<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="style.css">

</head>
<style>
	.btn-outline-success
	{
        padding: 5px 20px 5px 20px;
	    min-width: 80px;
	    font-size: 12px;
	    float: right;
	    text-transform: uppercase;
	    font-weight: 300;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	    letter-spacing: 2px;
	    height: 32px;
	}
</style>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" 
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <button class="btn" href="home.php"><i class="fa fa-home"></i></button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    	<ul class="navbar-nav">
			          <li class="nav-item active">
			            <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
			          </li>
			          <li class="nav-item active">
			            <a class="nav-link" href="logout.php">Logout</a>
			          </li>
                    <ul class="navbar-nav">                    
                    <li class="nav-item active">
                 
                  </li>
                </ul>
                <div>
                	<?php 
                	    $count=0;
                        if(isset($_SESSION['cart']))
                        {
                        	$count=count($_SESSION['cart']);
                        }
                	?>
                <a href="headsamsungcart.php" class="btn btn-outline-success">My Cart (<?php echo $count; ?>)</a>
                </div>
              </div>
            </nav>
     
				<div class="container mt-4">
					<div class="row">
					<div class="col-lg-3">
					<form action="manageheadsamsung.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="headsam1.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Level On Wireless, Black Sapphire</h6>
						    <p class="card-text">Price: Rs.1999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Level On Wireless, Black Sapphire">
						    <input type="hidden" name="Price" value="1999">						    
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageheadsamsung.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="headsam2.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Delivers Studio-Quality Sound with New Premium Headphones from AKG</h6>
						    <p class="card-text">Price: Rs.2999</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Delivers Studio-Quality Sound with New Premium Headphones from AKG">
						    <input type="hidden" name="Price" value="2999">
						    
						    </div>
						</div>
					</form>
				</div>

               

				<div class="col-lg-3">
					<form action="manageheadsamsung.php" method="post">
						<div class="card" style="width: 10rem;">
						  <img src="headsam5.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Level Headphones</h6>
						    <p class="card-text">Price: Rs.1550</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Level Headphones">
						    <input type="hidden" name="Price" value="1550">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageheadsamsung.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;" >
						  <img src="headsam6.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">AKG Headphones</h6>
						    <p class="card-text">Price: Rs.1299</p>
						    <button type="submit"name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="AKG Headphones">
						    <input type="hidden" name="Price" value="1299">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageheadsamsung.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="headsam7.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">SAMSUNG AKG-Y500 Bluetooth Headset  (Black, On the Ear)</h6> <p class="card-text">Price: Rs.9999</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="SAMSUNG AKG-Y500 Bluetooth Headset  (Black, On the Ear)">
						    <input type="hidden" name="Price" value="9999">
						    </div>
						</div>
					</form>
				</div>

				<div class="col-lg-3">
					<form action="manageheadsamsung.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="headsam8.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">Samsung Wireless Earphones - Buy Samsung Wireless Earphones</h6>
						    <p class="card-text">Price: Rs.2000</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Wireless Earphones - Buy Samsung Wireless Earphones">
						    <input type="hidden" name="Price" value="2000">
						    </div>
						</div>
					</form>
				</div>
                
                <div class="col-lg-3">
					<form action="manageheadsamsung.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="headsam3.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title"> Samsung Delivers Studio-Quality Sound with New Premium Headphones from AKG</h6>
						    <p class="card-text">Price: Rs.1332</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="Samsung Delivers Studio-Quality Sound with New Premium Headphones from AKG">
						    <input type="hidden" name="Price" value="1332">
						    
						    </div>
						</div>
					</form>
				</div>


				<div class="col-lg-3">
					<form action="manageheadsamsung.php" method="post">
						<div class="card" style="width: 10rem; height: 22rem;">
						  <img src="headsam4.jpg" class="card-img-top" >
						    <div class="card-body">
						    <h6 class="card-title">SAMSUNG GALAXY WIRELESS HEADPHONES CONCEPT WAS DESIGNED TO RIVAL THE APPLE AIRPODS MAX</h6>
						    <p class="card-text">Price: Rs.1499</p>
						    <button type="submit" name="Add_to_Cart" class="btn btn-info">Add to Cart</button> 
						    <input type="hidden" name="Item_Name" value="SAMSUNG GALAXY WIRELESS HEADPHONES CONCEPT WAS DESIGNED TO RIVAL THE APPLE AIRPODS MAX">
						    <input type="hidden" name="Price" value="1499">
						    </div>
						</div>
					</form>
				</div>

				
</body>
</html>